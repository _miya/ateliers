module.exports = function (grunt) {

    grunt.initConfig({
        ts: {
            default: {
                tsconfig: './tsconfig.json',
                watch: 'src'
            }
        }
    });

    grunt.loadNpmTasks("grunt-ts");

    grunt.registerTask('default', ['ts']);

};