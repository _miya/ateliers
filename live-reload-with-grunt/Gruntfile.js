module.exports = function (grunt) {

    grunt.initConfig({
        connect: {
            server: {
                options: {
                    port: 9000,
                    base: 'src'
                }
            }
        },
        watch: {
            options: {
                livereload: true,
            },
            lessToCss: {
                files: 'src/less/*.less',
                tasks: ['less']
            }
        },
        less: {
            // development: {
            //     options: {
            //         compress: false,
            //         sourceMap: false
            //     },
            //     files: {
            //         'src/css/style.css': 'src/less/style.less'
            //     }
            // }
            development: {
                options: {
                    plugins: [
                        new (require('less-plugin-clean-css'))({
                            advanced: true,
                            compatibility: 'ie7'
                        })
                    ],
                },
                files: {
                    'src/css/style.css': 'src/less/style.less'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default', ['connect', 'watch']);

};