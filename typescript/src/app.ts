class User {
    id: number;
    name: string = '';

    constructor(id: number) {
        this.id = id;
    }
}

// interface User {
//     id: number;
//     name: string;
// }

class App {

    private name: string;

    constructor(name: string) {
        this.name = name;
    }

    getMyName(): string {
        return this.name;
    }

    run() {
        const users: User[] = [
            { id: 1, name: "John" },
            { id: 2, name: "Frank" },
            { id: 3, name: "Mary" }
        ];
        const usernames = users.map(user => user.name);
        console.log(usernames);
    }

    multiply(a: number, b: number = 10) {
        return a * b;
    }

    addAll(message: string, ...numbers: number[]) {
        console.log(message);
        return numbers.reduce((prev, next) => prev + next);
    }

    mergeArrays(arr1: User[], arr2: User[]) {
        return [...arr1, ...arr2];
    }

};


const pizza = {
    name: 'Pepperoni',
    toppings: ['pepperoni']
};

const response = {
    header: {
        type: 'json',
        code: 200
    },
    body: {
        key: "toto",
        content: "lorem ipsum"
    }
};

function order({ name: pizzaName, toppings: pizzaToppings }: any): void {
    console.log(pizzaName, pizzaToppings);
}

function orderArr([num1, num2, num3]: number[]): void {
    console.log(num1);
}

const [test1, test2, test3] = [1, 2, 3];

const pizzaName = pizza.name;

const { name: myPizzaName } = pizza;

const { header, body } = response;

console.log(`myPizzaName ${myPizzaName}`);



console.log(test1);

const app = new App("TS APP");
app.run();


const myString: string | null = null;
console.log(myString);





const user1 = new User(1);
console.log(user1.name);