var User = (function () {
    function User(id) {
        this.name = '';
        this.id = id;
    }
    return User;
}());
var App = (function () {
    function App(name) {
        this.name = name;
    }
    App.prototype.getMyName = function () {
        return this.name;
    };
    App.prototype.run = function () {
        var users = [
            { id: 1, name: "John" },
            { id: 2, name: "Frank" },
            { id: 3, name: "Mary" }
        ];
        var usernames = users.map(function (user) { return user.name; });
        console.log(usernames);
    };
    App.prototype.multiply = function (a, b) {
        if (b === void 0) { b = 10; }
        return a * b;
    };
    App.prototype.addAll = function (message) {
        var numbers = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            numbers[_i - 1] = arguments[_i];
        }
        console.log(message);
        return numbers.reduce(function (prev, next) { return prev + next; });
    };
    App.prototype.mergeArrays = function (arr1, arr2) {
        return arr1.concat(arr2);
    };
    return App;
}());
;
var pizza = {
    name: 'Pepperoni',
    toppings: ['pepperoni']
};
var response = {
    header: {
        type: 'json',
        code: 200
    },
    body: {
        key: "toto",
        content: "lorem ipsum"
    }
};
function order(_a) {
    var pizzaName = _a.name, pizzaToppings = _a.toppings;
    console.log(pizzaName, pizzaToppings);
}
function orderArr(_a) {
    var num1 = _a[0], num2 = _a[1], num3 = _a[2];
    console.log(num1);
}
var _a = [1, 2, 3], test1 = _a[0], test2 = _a[1], test3 = _a[2];
var pizzaName = pizza.name;
var myPizzaName = pizza.name;
var header = response.header, body = response.body;
console.log("myPizzaName " + myPizzaName);
console.log(test1);
var app = new App("TS APP");
app.run();
var myString = null;
console.log(myString);
var user1 = new User(1);
console.log(user1.name);
